# AFramework - An automated testing framework for API test.

## About
AFramework是一套通用的功能自动化测试框架，可以很方便的用于验收测试和验收测试驱动开发（ATTD）。
- AFramework使用易于使用、理解的YAML语法，易于上手。  
- AFramework提供plugin扩展方式，可以快速创建更高级的Action。   
- 编写YAML = 设计代码，很优雅

## Requirements
- Java (本人仅在java 1.7版本下开发完成，发布的release版本也都是在1.7版本下打包完成)

## Usage
```shell
java -jar AFramework.jar [--testcase-file=testcase.yml]
```
使用--help查看更多使用帮助。  

### Testcase
- 关于YAML语法可参见[YAML Syntax](http://docs.ansible.com/ansible/latest/YAMLSyntax.html)），这里有一个示例[testcase.yml](docs/github.yml)。
- 每一个YAML文件中包含了一个task列表（任务列表），一个task执行完毕之后，下一个task才会执行，每一个task必须有且仅有一个Action动作，每个task也需要指定一个名称name，这样在运行的过程中，从其输出的任务执行信息中可以很好的辨别出是属于哪一个task的，如果没有定义name，action的值将会用作输出信息标记特定的task。
- 如果你的task列表很多的时候，在一个YAML文件里面会使得这个文件特别大，可能你会希望这些文件是可以方便去重用的，此时使用include语句引用task文件的方法，可允许你将其分解到更小的文件中。
```YAML
- name: new task测试用例集
  include:
    name: new_task.yml
```
- 定义变量，在task列表运行过程中的任意地方你都可以使用defaults或者vars定义一个新的变量，那么后续的task运行时即可引用。
```YAML
- vars:
    username: haha
    password: baba
```
- 注册变量，变量的另一个用途是在运行命令时，把命令结果存储到一个变量中，此时使用register关键字即可达到注册变量的目的。
```YAML
- name: 登陆系统获取session
  requests:
    method: POST
    url: http://server/login
    basic_auth:
      - "{{ username }}"
      - "{{ password }}"
  register: session
```
- 条件选择when语句，有时候可能需要某一个task只在特定的条件下执行，如上例中想要在登陆成功后再执行某个task
```YAML
- name: 登陆成功后获取用户信息
  requests:
    method: POST
    url: http://server/info
  when: session.result.Code == "Success"
```
- 标准循环，为了保持简洁，重复的任务可以这么写：
```YAML
- name: 获取多个信息
  requests:
    method: POST
    url: "http://server/info/{{ item }}"
  with_items:
    - Bob
    - Wheel
```
  如果你在前面定义了变量是一个可循环的值，那么你也可以这样做：
```YAML
with_items: "{{ somelist }}"
```
- Do-Util循环，有时你想重试一个任务直到达到某个条件：
```YAML
- name: 异步请求直至结束
  requests:
    method: POST
    url: "http://server/async_task"
  retries: 20
  delay: 3
  util: result.Code == "Finished"
```
- assert断言语句，判断一个task最终执行的结果pass还是fail，在最终的结果统计中也会以此来统计。
```YAML
- requests:
    method: POST
    url: http://server/info
  assert: result.Code == "Success"
```
- Action列表：
  - debug(debug用，用于打印一个变量的值或者一个msg)
  - defaults (定义变量值，若已存在则不改变)
  - requests (HTTP request调用)
  - shell (shell调用)
  - vars (定义变量，若已存在则会覆盖)

### Plugins
开发一个插件需要指定包名为com.github.meanstrong.aframework.plugins.action，类必须继承自ActionBase，复写其中的run方法，如下所示：   
```java
package com.github.meanstrong.aframework.plugins.action;
public class NewAction extends ActionBase {
    public NewAction(Task task) {
		super(task);
	}

    @Override
	public Map<String, Object> run(Map<String, Object> task_vars) {
        Object args = this.task.args; //args是task中Action对应的参数，可以以map形式取出各值。
        // to do something
    }
}
```
将写好的插件生成jar后放置于requires目录下，运行AFramework时将自动加载。

## Release
- [AFramework-0.0.1.jar](https://github.com/meanstrong/AFramework-java/releases/download/v0.0.1/AFramework-0.0.1.jar)

## Author
- <a href="mailto:pmq2008@gmail.com">Rocky Peng</a>

> AFramework Python版本正在开发中，预计近期也会发布出来。
