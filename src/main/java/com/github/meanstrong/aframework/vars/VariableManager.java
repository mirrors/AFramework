package com.github.meanstrong.aframework.vars;

import java.util.HashMap;
import java.util.Map;

public class VariableManager {
	private Map<String, Object> extra_vars;
	private Map<String, Object> playbook_vars;
	private Map<String, Object> task_vars;
	public VariableManager(){
		this.playbook_vars = new HashMap<String, Object>();
		this.task_vars = new HashMap<String, Object>();
		this.extra_vars = new HashMap<String, Object>();
		this.set_extra_var("false", false);
		this.set_extra_var("False", false);
		this.set_extra_var("FALSE", false);
		this.set_extra_var("true", true);
		this.set_extra_var("True", true);
		this.set_extra_var("TRUE", true);
		this.set_extra_var("null", null);
		this.set_extra_var("Null", null);
		this.set_extra_var("NULL", null);
		this.set_extra_var("None", null);
	}
	public VariableManager(Map<String, Object> extra_vars){
		this();
		this.extra_vars.putAll(extra_vars);
	}
	public void set_extra_var(String key, Object value){
		this.extra_vars.put(key, value);
	}
	public Object get_var(String key){
		return this.extra_vars.get(key);
	}
	public void set_playbook_vars(Map<String, Object> playbook_vars){
		if(playbook_vars != null) {
			this.playbook_vars.putAll(playbook_vars);
		}
	}
	public void set_playbook_var(String key, Object value){
		this.playbook_vars.put(key, value);
	}
	public void set_task_vars(Map<String, Object> task_vars){
		if(task_vars != null) {
			this.playbook_vars.putAll(task_vars);
		}
	}
	public void set_task_var(String key, Object value){
		this.playbook_vars.put(key, value);
	}
	public boolean contains(String name){
		if (this.playbook_vars.containsKey(name) || this.task_vars.containsKey(name) || this.extra_vars.containsKey(name)) {
			return true;
		}
		return false;
	}
	public Map<String, Object> all_vars(){
		Map<String, Object> all_vars = new HashMap<String, Object>();
		all_vars.putAll(this.playbook_vars);
		all_vars.putAll(this.task_vars);
		all_vars.putAll(this.extra_vars);
		return all_vars;
	}
}
