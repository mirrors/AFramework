package com.github.meanstrong.aframework;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.github.meanstrong.aframework.plugins.Loader;
import com.github.meanstrong.aframework.executor.Playbook;
import com.github.meanstrong.aframework.result.Result;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hubspot.jinjava.Jinjava;
import com.hubspot.jinjava.interpret.Context;
import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.interpret.RenderResult;

public class PlaybookCLI {

	public static void main(String[] args) throws FileNotFoundException {
		String testcase_file = "testcase.yml";
		String requires = "requires";
		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("--testcase-file=")) {
				testcase_file = args[i].split("=")[1];
			} else if (args[i].startsWith("--requires")) {
				requires = args[i].split("=")[1];
			} else if (args[i].equalsIgnoreCase("-h") || args[i].equalsIgnoreCase("--help")) {
				PlaybookCLI.print_help();
				return;
			}
		}
		if (testcase_file.length() == 0) {
			PlaybookCLI.print_help();
			return;
		}
		if (requires.length() > 0) {
			Loader loader = new Loader("");
			loader.load_jar(requires);
		}

		// Result result = new Result();
		Playbook executor = new Playbook(testcase_file);
		executor.run();
		System.out.println(executor.get_result());
	}

	public static void print_help() {
		System.out.println("Usage: java -jar AFramework.jar --testcase-file=testcase.yml --requires=/requires/path/");
		System.out.println("Options:");
		System.out.println("\t--testcase-file\tThe testcase file.");
		System.out.println("\t--requires\tThe requires jar or zip file path.");
	}
	
	public static void test(){
		Jinjava jinjava = new Jinjava();
		HashMap<String, Object> bind = Maps.newHashMap();
		ArrayList<Object> list_a = Lists.newArrayList();
		list_a.add("1");
		list_a.add("2");
		bind.put("ddd", "sssss");
		RenderResult x = jinjava.renderForResult("{{ ddd }}", bind);
		System.out.println(x.getContext().keySet());
		JinjavaInterpreter interpreter = jinjava.newInterpreter();
		Context context = new Context(interpreter.getContext(), bind);
		System.out.println(interpreter.resolveELExpression(" ddd && 12  ", -1));
	}
}
