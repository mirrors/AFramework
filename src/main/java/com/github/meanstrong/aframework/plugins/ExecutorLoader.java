package com.github.meanstrong.aframework.plugins;

import java.lang.reflect.Constructor;
import java.util.Map;

import com.github.meanstrong.aframework.parsing.DataLoader;
import com.github.meanstrong.aframework.playbook.Task;
import com.github.meanstrong.aframework.result.Result;


public class ExecutorLoader {
	private String pkg;
	
	public ExecutorLoader(){
		String current_pkg = this.getClass().getPackage().getName();
		this.pkg = current_pkg.substring(0, current_pkg.length()-"plugins".length())+"executor";
	}
	
//	public ActionBase get(String name, Task task){
//		name = name.substring(0, 1).toUpperCase() + name.substring(1);
//		ActionBase handler = null;
//		try {
//			Class<?> clazz = Class.forName(this.pkg+"."+name);
//			Constructor<?> constructor = clazz.getConstructor(Task.class);
//			handler = (ActionBase) constructor.newInstance(task);
//		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//			// TODO Auto-generated catch block
//		}
//		return handler;
//	}

	public boolean contains(String name){
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		Class<?> clazz;
		try {
			clazz = Class.forName(this.pkg+"."+name);
			Constructor<?> constructor = clazz.getConstructor(Task.class, Map.class, Result.class, DataLoader.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
		return true;
	}
}
