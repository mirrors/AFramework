package com.github.meanstrong.aframework.plugins.action;

import java.util.Map;

import com.github.meanstrong.aframework.playbook.Task;

public class Defaults extends ActionBase {

	public Defaults(Task task) {
		super(task);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object run(Map<String, Object> task_vars) {
		for (Map.Entry<String, Object> entry : ((Map<String, Object>) this.task.args).entrySet()) {
			if (!this.task.get_variable_manager().contains(entry.getKey())) {
				this.task.get_variable_manager().set_playbook_var(entry.getKey(), entry.getValue());
			}
		}
		return null;
	}

}
