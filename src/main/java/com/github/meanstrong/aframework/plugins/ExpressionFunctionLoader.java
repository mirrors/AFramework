package com.github.meanstrong.aframework.plugins;

import java.lang.reflect.Constructor;

import com.github.meanstrong.aframework.expression.function.Function;


public class ExpressionFunctionLoader {
	private String pkg;
	
	public ExpressionFunctionLoader(){
		String current_pkg = this.getClass().getPackage().getName();
		this.pkg = current_pkg.substring(0, current_pkg.length()-"plugins".length())+"expression.function";
	}
	
	public Function get(String name){
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		Function handler = null;
		try {
			Class<?> clazz = Class.forName(this.pkg+"."+name);
			Constructor<?> constructor = clazz.getConstructor();
			handler = (Function) constructor.newInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return handler;
	}

	public boolean contains(String name){
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		Class<?> clazz;
		try {
			clazz = Class.forName(this.pkg+"."+name);
			Constructor<?> constructor = clazz.getConstructor(com.github.meanstrong.aframework.playbook.Task.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
		return true;
	}
}
