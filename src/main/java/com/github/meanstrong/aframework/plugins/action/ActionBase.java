package com.github.meanstrong.aframework.plugins.action;

import java.util.Map;

import com.github.meanstrong.aframework.playbook.Task;

public abstract class ActionBase {
	public Task task;

	public ActionBase(Task task) {
		this.task = task;
	}

	// 其他类覆盖实现此方法
	public abstract Object run(Map<String, Object> task_vars);
	// Object args = this.task.args;
	// Map<String, Object> result = new HashMap<String, Object>();
	//// System.out.println("TestcaseRunner");
	//// System.out.println(args);
	// Map<String, Object> testcase = (Map<String, Object>) args;
	// result.put("testcase", testcase);
	// return result;
	// }

}
