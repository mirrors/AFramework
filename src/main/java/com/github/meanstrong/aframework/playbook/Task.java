package com.github.meanstrong.aframework.playbook;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.meanstrong.aframework.expression.Expression;
import com.github.meanstrong.aframework.parsing.DataLoader;
import com.github.meanstrong.aframework.plugins.ActionLoader;
import com.github.meanstrong.aframework.plugins.ExecutorLoader;
import com.github.meanstrong.aframework.template.Templar;
import com.github.meanstrong.aframework.vars.VariableManager;
import com.google.common.collect.Maps;
import com.hubspot.jinjava.Jinjava;

public class Task {
	private VariableManager variable_manager;
	private DataLoader _loader;
	private Map<String, Object> _ds;
	private Map<String, Object> _attributes;
	private Map<String, Object> _valid_attrs;
	public String action;
	public Object args;
	public String name;
	public int retries;
	public double delay;
	public String when;
	public String util;
	public String register;
	public String assert_;
	public String loop_var;
	public Object loop_args;

	public Task(DataLoader loader) {
		this._loader = loader;
		this._attributes = new HashMap<String, Object>();
		this._attributes.put("name", "");
		this._attributes.put("retries", 1);
		this._attributes.put("delay", 1);
		this._attributes.put("when", "");
		this._attributes.put("util", "");
		this._attributes.put("register", "");
		this._attributes.put("assert", "");
		this._attributes.put("action", "");
		this._attributes.put("args", "");
		this._valid_attrs = new HashMap<String, Object>();
		this._valid_attrs.put("name", "");
		this._valid_attrs.put("retries", 1);
		this._valid_attrs.put("delay", 1);
		this._valid_attrs.put("when", "");
		this._valid_attrs.put("util", "");
		this._valid_attrs.put("register", "");
		this._valid_attrs.put("assert", "");
		this._valid_attrs.put("action", "");
		this._valid_attrs.put("args", "");
		this._valid_attrs.put("loop_var", "");
		this._valid_attrs.put("loop_args", "");
	}

	public Object get_attr(String name) {
		if (this._attributes.containsKey(name)) {
			return this._attributes.get(name);
		}
		throw new RuntimeException("Task has no attributes " + name);
	}

	public static Task load(Map<String, Object> data, VariableManager variable_manager, DataLoader loader) {
		Task task = new Task(loader);
		return task.load_data(data, variable_manager);
	}

	public Task load_data(Map<String, Object> data, VariableManager variable_manager) {
		this.variable_manager = variable_manager;
		// this._ds = data;
		Map<String, Object> new_ds = this.process_data(data);
		if (new_ds.get("action") == null) {
			throw new RuntimeException("action is invalid: " + new_ds.toString());
		}
		for (String key : this._valid_attrs.keySet()) {
			if (new_ds.get(key) != null) {
				this._attributes.put(key, new_ds.get(key));
			}
		}
		return this;
	}

	public VariableManager get_variable_manager() {
		return this.variable_manager;
	}

	public Map<String, Object> process_data(Map<String, Object> data) {
		Map<String, Object> new_ds = new HashMap<String, Object>();
		new_ds.putAll(data);
		ActionLoader action_loader = new ActionLoader();
		ExecutorLoader executor_loader = new ExecutorLoader();
		for (String key : data.keySet()) {
			if (action_loader.contains(key) || executor_loader.contains(key)) {
				new_ds.put("action", key);
				new_ds.put("args", data.get(key));
			}
			if (key.startsWith("with_")) {
				new_ds.put("loop_var", key.substring(5));
				new_ds.put("loop_args", data.get(key));
			}
		}
		return new_ds;
	}

	public void validate(Templar templar) {
		this.action = (String) this._attributes.get("action");
		this.name = (String) templar.template(this._attributes.get("name"));
		this.register = (String) templar.template(this._attributes.get("register"));
		this.loop_var = (String) this._attributes.get("loop_var");
		this.loop_args = this._validate_loop_args(null, this._attributes.get("loop_args"), templar);
	}

	public void post_validate(Templar templar) {
		Object value;
		for (String key : this._valid_attrs.keySet()) {
			try {
				Method method = this.getClass().getMethod("_validate_" + key, Object.class, Object.class,
						Templar.class);
				value = method.invoke(this, null, this._attributes.get(key), templar);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				value = templar.template(this._attributes.get(key));
			}
			if (key.equals("assert")) {
				this.assert_ = (String) value;
			} else {
				Field field;
				try {
					field = this.getClass().getField(key);
					field.set(this, value);
				} catch (NoSuchFieldException | SecurityException | IllegalArgumentException
						| IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	public String get_assert() {
		return (String) this.assert_;
	}

	public Object _validate_assert(Object attr, Object value, Templar templar) {
		return value;
	}

	public Object _validate_util(Object attr, Object value, Templar templar) {
		return value;
	}

	public Object _validate_loop_var(Object attr, Object value, Templar templar) {
		return value;
	}

	public Object _validate_args(Object attr, Object value, Templar templar) {
		return value;
	}

	public Object _validate_loop_args(Object attr, Object value, Templar templar) {
		if (value instanceof List) {
			return value;
		} else if (value instanceof String) {
			return templar.template(value);
		} else {
			return value;
			// throw new RuntimeException("unknow loop args: "+value);
		}
	}

	public static boolean eval_condition(String string, Map<String, Object> vars) {
		Templar templar = new Templar(vars);
		if (string.contains("{{")) {
			string = templar.template(string).toString();
		}
		string = "{{" + string + "}}";
		Object object = templar.template(string);
		if (object == null) {
			return false;
		}

		if (object instanceof Boolean) {
			Boolean b = (Boolean) object;
			return b.booleanValue();
		}

		if (object instanceof Number) {
			return ((Number) object).intValue() != 0;
		}

		if (object instanceof String) {
			return !"".equals(object) && !"false".equalsIgnoreCase((String) object);
		}

		if (object.getClass().isArray()) {
			return Array.getLength(object) != 0;
		}

		if (object instanceof Collection) {
			return ((Collection<?>) object).size() != 0;
		}

		if (object instanceof Map) {
			return ((Map<?, ?>) object).size() != 0;
		}

		return true;
	}

	public DataLoader get_loader() {
		return this._loader;
	}

	@Override
	public String toString() {
		String desc = this.name;
		if (desc.isEmpty()) {
			desc = this.action;
		}
		return desc;
	}
}
