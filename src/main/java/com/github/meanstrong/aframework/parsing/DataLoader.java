package com.github.meanstrong.aframework.parsing;

import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;

public class DataLoader {
	private Path _basedir;

	public DataLoader() {
		this._basedir = Paths.get(".").toAbsolutePath().normalize();
	}

	public List<Map<String, Object>> load_from_file(String filename) {
		File file = this._basedir.resolve(filename).toFile();
		Yaml yaml = new Yaml();
		try {
			return (List<Map<String, Object>>) yaml.load(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
	}

	public void set_base_dir(Path basedir) {
		this._basedir = basedir;
	}

	public Path get_base_dir() {
		return this._basedir;
	}
}
