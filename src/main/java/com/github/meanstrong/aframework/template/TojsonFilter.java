package com.github.meanstrong.aframework.template;

import com.alibaba.fastjson.JSONObject;
import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.lib.filter.Filter;

public class TojsonFilter implements Filter {

	  @Override
	  public String getName() {
	    return "to_json";
	  }

	  @Override
	  public Object filter(Object var, JinjavaInterpreter interpreter, String... args) {
		  JSONObject result = JSONObject.parseObject((String) var);
		  return result;
	  }
}
