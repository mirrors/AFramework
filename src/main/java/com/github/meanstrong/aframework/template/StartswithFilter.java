package com.github.meanstrong.aframework.template;

import com.hubspot.jinjava.interpret.InterpretException;
import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.lib.filter.Filter;

public class StartswithFilter implements Filter {

	@Override
	public String getName() {
		return "startswith";
	}

	@Override
	public Object filter(Object object, JinjavaInterpreter interpreter, String... args) {
		if (args.length == 0) {
			throw new InterpretException(getName() + " takes at least 1 argument.", interpreter.getLineNumber());
		}
		if (object instanceof String) {
			String value = (String) object;
			return value.startsWith(args[0]);
		}
		return false;
	}
}
