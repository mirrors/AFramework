package com.github.meanstrong.aframework.executor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.github.meanstrong.aframework.parsing.DataLoader;
import com.github.meanstrong.aframework.plugins.ActionLoader;
import com.github.meanstrong.aframework.plugins.action.ActionBase;
import com.github.meanstrong.aframework.result.Result;
import com.github.meanstrong.aframework.template.Templar;

public class Task {
	protected com.github.meanstrong.aframework.playbook.Task task;
	protected Map<String, Object> task_vars;
	protected Result result;
	protected DataLoader _loader;

	public Task(com.github.meanstrong.aframework.playbook.Task task, Map<String, Object> job_vars, Result result,
			DataLoader loader) {
		this.task = task;
		this.task_vars = job_vars;
		this.result = result;
		this._loader = loader;
	}

	public Map<String, Object> run() {
		Map<String, Object> result = new HashMap<String, Object>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms");
		String start = "";
		String end = "";
		try {
			start = format.format(new Date());
			this.result.start_test(this.task);
			List<Object> items = this._get_loop_items();
			result = this._run_loop(items);
			if (!this.task.register.isEmpty()) {
				this.task.get_variable_manager().set_playbook_var(this.task.register, result);
			}
			if (result.containsKey("skipped") && ((boolean) result.get("skipped"))) {
				this.result.add_skipped(this.task);
			} else if (result.containsKey("failed") && ((boolean) result.get("failed"))) {
				if (result.get("exception") == null) {
					this.result.add_failure(this.task, (String) result.get("msg"));
				} else {
					this.result.add_error(this.task, (String) result.get("exception"));
				}
			} else {
				this.result.add_success(this.task);
			}
		} catch (Exception e) {
			result.put("failed", true);
			result.put("msg", "Unexpected failure during task execution.");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			result.put("traceback", errors.toString());
			result.put("exception", e.getMessage());
			this.result.add_error(this.task, errors.toString());
		} finally {
			end = format.format(new Date());
			this.result.stop_test(this.task);
		}
		result.put("start", start);
		result.put("end", end);
		// debug msg...
		System.out.println("Task <" + this.task + "> result:");
		System.out.println(JSON.toJSONString(result, true));
		return result;
	}

	public List<Object> _get_loop_items() {
		if (this.task.loop_args == null) {
			return null;
		} else if (this.task.loop_args instanceof List) {
			return (List<Object>) this.task.loop_args;
		} else {
			throw new RuntimeException("loop args MUST list, but it is " + this.task.loop_args.getClass());
		}
	}

	public Map<String, Object> _run_loop(List<Object> items) {
		Map<String, Object> result = null;
		if (items == null) {
			result = this._execute();
		} else {
			result = new HashMap<String, Object>();
			List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
			result.put("failed", false);
			for (Object item : items) {
				Map<String, Object> res = null;
				this.task_vars.put("item", item);
				res = this._execute();
				res.put("item", item);
				results.add(res);
				if ((boolean) res.get("failed")) {
					result.put("failed", true);
				}
			}
			result.put("results", results);
		}
		return result;
	}

	public Map<String, Object> _execute() {
		// Map<String, Object> task_vars = new HashMap<String, Object>();
		this.task_vars.putAll(this.task.get_variable_manager().all_vars());
		Templar templar = new Templar(this.task_vars);
		this.task.post_validate(templar);
		int retries = this.task.retries;
		double delay = this.task.delay;
		Map<String, Object> result = new HashMap<String, Object>();
		if (!this.task.when.isEmpty() && !this.task.eval_condition(this.task.when, this.task_vars)) {
			result.put("failed", false);
			result.put("skipped", true);
			return result;
		}
		String action = this.task.action.substring(0, 1).toUpperCase() + this.task.action.substring(1);
		ActionBase handler = this.get_action_handler(action);
		if (handler == null) {
			result.put("failed", true);
			result.put("msg", "cannot get action handler by named: " + action);
			return result;
		}
		result.put("failed", false);
		this.task.args = templar.template(this.task.args);
		try {
			for (int i = 0; i < retries; i++) {
				Object res = handler.run(this.task_vars);
				this.task_vars.put("result", res);
				result.put("result", res);
				if (!this.task.util.isEmpty() && this.task.eval_condition(this.task.util, this.task_vars)) {
					break;
				}
				Thread.sleep((int) delay * 1000);
			}
			if (!this.task.get_assert().isEmpty()
					&& !this.task.eval_condition(this.task.get_assert(), this.task_vars)) {
				result.put("failed", true);
				result.put("msg", "assertion error.");
			}
		} catch (Exception e) {
			result.put("failed", true);
			result.put("msg", "Unexpected failure during action handler execution.");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			result.put("exception", errors.toString());
		}
		// if (retries > 1) {
		// result.put("retries", retries);
		// }
		return result;
	}

	public ActionBase get_action_handler(String name) {
		ActionLoader loader = new ActionLoader();
		ActionBase handler = loader.get(name, this.task);
		return handler;
	}
}
