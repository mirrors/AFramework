package com.github.meanstrong.aframework.expression.parser;

public class ParserVariable extends Parser{

	public Result parse(String target){
		if(!(Character.isLetter(target.charAt(0)) || target.charAt(0) == '_')){
			return Result.fail();
		}
		int step = 1;
		while(step < target.length() && (Character.isLetterOrDigit(target.charAt(step))
			  || target.charAt(step) == '_')){
			step += 1;
		}
		return Result.succeed(target.substring(0, step), target.substring(step));
	}
}
