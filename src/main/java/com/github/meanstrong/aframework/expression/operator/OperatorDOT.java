package com.github.meanstrong.aframework.expression.operator;

import java.util.List;
import java.util.Map;

import com.github.meanstrong.aframework.expression.Constant;

public class OperatorDOT extends Operator {
	public OperatorDOT(){
		super(".", 1, 2);
	}
	public Constant evaluate(List<Constant> args){
		super.check_args_num(args.size());
		if(args.get(0).get_value() instanceof Map){
			return new Constant(((Map<?, ?>) args.get(0).get_value()).get(args.get(1).get_value()));
		}else if(args.get(0).get_value() instanceof List && args.get(1).get_value() instanceof Integer){
			return new Constant(((List<?>) args.get(0).get_value()).get((int) args.get(1).get_value()));
		}
		throw new RuntimeException("OperatorDOT args error: "+args.get(0)+" and "+args.get(1));
	}
}
