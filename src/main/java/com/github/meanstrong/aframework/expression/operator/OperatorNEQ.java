package com.github.meanstrong.aframework.expression.operator;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class OperatorNEQ extends Operator {
	OperatorNEQ(){
		super("!=", 8, 2);
	}
	public Constant evaluate(List<Constant> args){
		super.check_args_num(args.size());
		boolean result = false;
		if (args.get(0).get_value() == null || args.get(1).get_value() == null){
			result = !(args.get(0).get_value() == args.get(1).get_value());
		} else{
			result = !args.get(0).get_value().equals(args.get(1).get_value());
		}
		return new Constant(result);
	}
}
