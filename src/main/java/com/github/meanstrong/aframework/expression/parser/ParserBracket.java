package com.github.meanstrong.aframework.expression.parser;

public class ParserBracket extends Parser{
	public Result parse(String target){
		if(target.charAt(0) == '(' || target.charAt(0) == ')' || target.charAt(0) == '[' || target.charAt(0) == ']'){
			return Result.succeed(target.substring(0, 1), target.substring(1));
		}
		return Result.fail();
	}
}
