package com.github.meanstrong.aframework.expression.parser;

public class ParserFunction extends Parser{

	public Result parse(String target){
		if (target.charAt(0) != '$'){
			return Result.fail();
		}
		Result result = (new ParserVariable()).parse(target.substring(1));
		if(!result.is_succeeded()){
			return Result.fail();
		}
		int step = 1 + result.get_recognized().length();
		if(target.charAt(step) != '('){
			return Result.fail();
		}
		step += 1;
		while(target.charAt(step) != ')'){
			step += 1;
		}
		step += 1;
		return Result.succeed(target.substring(0, step), target.substring(step) );
	}
}
