package com.github.meanstrong.aframework.expression;

import java.util.List;
import java.util.Map;

public class Constant {
//	public enum DataType {
//		DATATYPE_NULL,
//		DATATYPE_STRING,
//		DATATYPE_BOOLEAN,
//		DATATYPE_INT,
//		DATATYPE_FLOAT ,
//		DATATYPE_LIST,
//		DATATYPE_DICT,
//		DATATYPE_VARIABLE,
//	}
	private Object _value;
	private boolean _is_variable;
	public Constant(Object value){
		this._value = value;
		this._is_variable = false;
	}
	public Constant(Object value, boolean is_variable){
		this._value = value;
		this._is_variable = is_variable;
	}
	public boolean is_variable(){
		return this._is_variable;
	}
	public Object get_value(){
		return this._value;
	}
//	public String get_string_data(){
//		return (String)this.data_value;
//	}
//	public int get_int_data(){
//		return (int)this.data_value;
//	}
//	public List get_list_data(){
//		return (List)this.data_value;
//	}
//	public Map get_dict_data(){
//		return (Map)this.data_value;
//	}
	public boolean get_value_boolean(){
		if(this._value instanceof Boolean){
			return (boolean)this._value;
		}
		if(this._value instanceof Integer){
			return (int)this._value != 0;
		}
		if(this._value instanceof Float){
			return (float)this._value != 0;
		}
		if(this._value instanceof List){
			return !((List<?>) this._value).isEmpty();
		}
		if(this._value instanceof Map){
			return !((Map<?, ?>) this._value).isEmpty();
		}
		throw new RuntimeException("Unknow constant data value: "+this._value);
	}
	public String toString(){
		String var = "";
		Object value = this._value;
		if(this._is_variable){
			var = "[Variable]";
		}else if(value instanceof String){
			value = "\""+((String) value)+"\"";
		}
		return String.format("<Constant%s %s>", var, value);
	}
}
