package com.github.meanstrong.aframework.expression.function;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class Startswith extends Function{
	public Startswith(){
		super("startswith", 2);
	}
	@Override
	public Constant call(List<Constant> args){
		super.check_args_num(args.size());
		if(args.get(0).get_value() instanceof String && args.get(1).get_value() instanceof String ){
			String arg0 = (String) args.get(0).get_value();
			String arg1 = (String) args.get(1).get_value();
			return new Constant(arg0.startsWith(arg1));
		}
		throw new RuntimeException("Function startswith args MUST all string.");
	}
}
