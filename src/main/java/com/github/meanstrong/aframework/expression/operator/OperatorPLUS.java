package com.github.meanstrong.aframework.expression.operator;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class OperatorPLUS extends Operator {
	OperatorPLUS(){
		super("+", 5, 2);
	}
	public Constant evaluate(List<Constant> args){
		super.check_args_num(args.size());
		if(args.get(0).get_value() instanceof Number && args.get(1).get_value() instanceof Number ){
			int result = (int)args.get(0).get_value() + (int)args.get(1).get_value();
			return new Constant(result);
		}else if(args.get(0).get_value() instanceof String && args.get(1).get_value() instanceof String ){
			String result = (String)args.get(0).get_value() + (String)args.get(1).get_value();
			return new Constant(result);
		}
		throw new RuntimeException("OperatorPLUS args error.");
	}
}
