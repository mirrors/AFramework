package com.github.meanstrong.aframework.expression.parser;

public class Result {
	private String recognized;
	private String remaining;
	private boolean succeeded;
	Result(String recognized, String remaining, boolean succeeded){
        this.recognized = recognized;
        this.remaining = remaining;
        this.succeeded = succeeded;
	}
    public boolean is_succeeded(){
        return this.succeeded;
    }
    public String get_recognized(){
        return this.recognized;
    }
    public String get_remaining(){
        return this.remaining;
    }
    public static Result succeed(String recognized, String remaining){
        return new Result(recognized, remaining, true);
    }
    public static Result fail(){
        return new Result("", "", false); 
    }
}
