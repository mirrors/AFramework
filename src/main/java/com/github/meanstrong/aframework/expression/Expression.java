package com.github.meanstrong.aframework.expression;

import java.util.ArrayList;
import java.util.List;

import com.github.meanstrong.aframework.expression.function.Function;
import com.github.meanstrong.aframework.expression.operator.Operator;
import com.github.meanstrong.aframework.expression.operator.OperatorDOT;
import com.github.meanstrong.aframework.expression.operator.OperatorLoader;
import com.github.meanstrong.aframework.expression.parser.Parser;
import com.github.meanstrong.aframework.expression.parser.ParserLoader;
import com.github.meanstrong.aframework.expression.parser.Result;
import com.github.meanstrong.aframework.plugins.ExpressionFunctionLoader;
import com.github.meanstrong.aframework.vars.VariableManager;

public class Expression {
	private VariableManager variable_manager;

	public Expression(VariableManager variable_manager) {
		this.variable_manager = variable_manager;
	}

	public static List<String> analyze(String expression) {
		List<String> stack = new ArrayList<String>();
		while (expression.length() > 0) {
			if (Character.isWhitespace(expression.charAt(0))) {
				expression = expression.substring(1);
				continue;
			}
			boolean is_succeed = false;
			for (Parser parser : ParserLoader.all()) {
				Result result = parser.parse(expression);
				is_succeed = result.is_succeeded();
				if (is_succeed) {
					stack.add(result.get_recognized());
					expression = result.get_remaining();
					break;
				}
			}
			if (!is_succeed) {
				throw new RuntimeException("All parser not recognized expression: " + expression);
			}
		}
		return stack;
	}

	public static List<Object> build_post_expression_stack(String expression) {
		List<Object> stack = new ArrayList<Object>();
		List<Object> op_stack = new ArrayList<Object>();
		for (String expr : Expression.analyze(expression)) {
			if (expr.charAt(0) == '"' || expr.charAt(0) == '\'') {
				Constant constant = new Constant(expr.substring(1, expr.length() - 1));
				stack.add(constant);
			} else if (expr.charAt(0) == '$') {
				int index = 0;
				while (expr.charAt(index) != '(') {
					index += 1;
				}
				String sub_string = expr.substring(index + 1, expr.length() - 1);
				for (String arg : sub_string.split(",")) {
					List<Object> s = Expression.build_post_expression_stack(arg);
					stack.addAll(s);
				}
				Function func = (new ExpressionFunctionLoader()).get(expr.substring(1, index));
				stack.add(func);
			} else if (Character.isLetter(expr.charAt(0))) {
				stack.add(new Constant(expr,
						!(op_stack.size() > 0 && op_stack.get(op_stack.size() - 1) instanceof OperatorDOT)));
			} else if (Character.isDigit(expr.charAt(0))) {
				stack.add(new Constant(Integer.parseInt(expr)));
			} else if (expr.equals("(")) {
				op_stack.add(expr);
			} else if (expr.equals(")")) {
				while (!op_stack.get(op_stack.size() - 1).equals("(")) {
					stack.add(op_stack.remove(op_stack.size() - 1));
				}
				op_stack.remove(op_stack.size() - 1);
			} else if (expr.equals("[")) {
				Operator op = new OperatorDOT();
				while (op_stack.size() > 0 && op_stack.get(op_stack.size() - 1) instanceof Operator
						&& ((Operator) op_stack.get(op_stack.size() - 1)).get_priority() <= op.get_priority()) {
					stack.add(op_stack.remove(op_stack.size() - 1));
				}
				op_stack.add(op);
				op_stack.add(expr);
			} else if (expr.equals("]")) {
				while (!op_stack.get(op_stack.size() - 1).equals("[")) {
					stack.add(op_stack.remove(op_stack.size() - 1));
				}
				op_stack.remove(op_stack.size() - 1);
			} else if (OperatorLoader.get(expr) != null) {
				Operator op = OperatorLoader.get(expr);
				while (op_stack.size() > 0 && op_stack.get(op_stack.size() - 1) instanceof Operator
						&& ((Operator) op_stack.get(op_stack.size() - 1)).get_priority() <= op.get_priority()) {
					stack.add(op_stack.remove(op_stack.size() - 1));
				}
				op_stack.add(op);
			} else {

			}
		}
		while (op_stack.size() > 0) {
			stack.add(op_stack.remove(op_stack.size() - 1));
		}
		return stack;
	}

	public Constant evaluate(String expression) {
		List<?> stack = Expression.build_post_expression_stack(expression);
		List<Constant> value = new ArrayList<Constant>();
		for (Object v : stack) {
			if (v instanceof Constant) {
				if (((Constant) v).is_variable()) {
					String key = (String) ((Constant) v).get_value();
					v = new Constant(this.variable_manager.get_var(key));
				}
				value.add((Constant) v);
			} else if (v instanceof Operator) {
				List<Constant> args = new ArrayList<Constant>();
				for (int i = 0; i < ((Operator) v).get_num_args(); i++) {
					Constant var = value.remove(value.size() - 1);
					args.add(0, var);
				}
				value.add(((Operator) v).evaluate(args));
			} else if (v instanceof Function) {
				List<Constant> args = new ArrayList<Constant>();
				for (int i = 0; i < ((Function) v).get_num_args(); i++) {
					Constant var = value.remove(value.size() - 1);
					args.add(0, var);
				}
				value.add(((Function) v).call(args));
			} else {
				throw new RuntimeException("RPN evaluate unknow stack " + v);
			}
		}
		if (value.size() != 1) {
			throw new RuntimeException("RPN evaluate error" + value);
		}
		return value.get(0);
	}

	public Object eval_value(String expression) {
		Constant result = this.evaluate(expression);
		return result.get_value();
	}

	public boolean eval_boolean(String expression) {
		Constant result = this.evaluate(expression);
		return result.get_value_boolean();
	}
}
