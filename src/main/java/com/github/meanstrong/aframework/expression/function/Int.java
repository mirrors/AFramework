package com.github.meanstrong.aframework.expression.function;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class Int extends Function{
	public Int(){
		super("int", 1);
	}
	@Override
	public Constant call(List<Constant> args){
		super.check_args_num(args.size());
		Object arg0 = args.get(0).get_value();
		if(arg0 instanceof String){
			return new Constant(Integer.parseInt((String) arg0));
		}else if(arg0 instanceof Integer){
			return new Constant((int) arg0);
		}
		throw new RuntimeException("Unknow function args type");
	}
}
